from setuptools import setup

setup(
    name='simple-example',
    version='0.0.1',
    description='My private package from bitbucket repo',
    url='git@bitbucket.org/swastik_biswas/simple_example.git',
    author='Swastik Biswas',
    author_email='swastik.biswas@bridgei2i.com',
    license='unlicense',
    packages=['simple-example'],
    zip_safe=False
)
